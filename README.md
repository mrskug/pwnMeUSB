PwnMe USB Flash Drive
=====================

# Table of Contents
1. Description
2. Compatibility
3. Setup
4. Waiver

## Description
Do you have a friend, a family member or a co-worker that continually forgets to lock their laptop when they step away from it? As technologists, we usually cringe when we see such things because we understand what the potential security implications of such a lapse are. However, many people simply do not. Now there is a way that you can gently, kindly, non-offensively and most important, non-destructively, remind those folks that it only takes a few seconds to pwn a laptop...which is ironically the same amount of time it takes to lock one. A video demonstration of this tool can be seen on D.Tube: https://d.tube/#!/v/gh057/9w9l45hy.

## Compatibility
Tested with OSX 10.13

## Setup
There are a few steps to getting everything configured properly. The idea is to create a flash drive that only has one option to a user; click the big, red button.

1. Copy the /source directory to the root of your flash drive.
2. Open the file /source/PwnMeScript.scpt in the application Script Editor. This is your "trigger". Export this Applescript as an application with "run only" checkbox checked.
3. Remember to change your MP3 and PNG files to something a bit more custom. To avoid any copyright issues, I have included only a very simple MP3 and PNG file so that everything still functions. You will want to change these to something that is better suited for your "victim". Also remember that the naming of the files is very important. Whatever you add, make sure to keep the names "audio.mp3" and "wallpaper.png". If you would like to customize these, you can do so in the /source/pwnage.sh bash script.
4. Rename the directory /source to /.source which is what the trigger is looking for. By putting a period character before the name of the directory, OSX will hide this directory. This keeps your source files tucked away nicely.
5. Test it out before you deploy it live. Nothing is worse than planning an prank and having it fall flat. Make sure everything works before you attempt things live.

## Waiver
I have created this script as a simple, non-invasive way to remind folks to keep security at the forefront of their minds. Nothing in the script as it is being distributed will do any harm to a user's computer. That being said, USE ONLY AT YOUR OWN RISK. If you do not know what a particular Unix command will do, DO NOT USE IT. With Unix, there is NO UNDO; keep that in mind. Further, please do not blame me if this backfires or causes damage. As I just said, USE AT YOUR OWN RISK.
